package com.gitlab.kynes.app;

public final class Config {
	public static final String ACL_REGEX = "(?<id>\\d+) from (?<source>.+) to (?<destination>.+) with (?<protocol>.+) => (?<action>.+)";

	private Config() {
	}

	public static final String ACL_EP = "acl";
	public static final String APP_NAME = "intelliment";
	public static final String ACL_FILE = "intelliment-devtest-acl.txt";
}
