package com.gitlab.kynes.app;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath( "/" )
public class RegistryApplication extends Application {
}
