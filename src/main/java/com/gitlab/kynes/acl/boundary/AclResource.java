package com.gitlab.kynes.acl.boundary;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.gitlab.kynes.acl.control.AclRepository;
import com.gitlab.kynes.acl.entity.Acl;

@Stateless
@Path( "acl" )
public class AclResource {

	@Inject
	AclRepository aclRepository;

	@GET
	@Produces( APPLICATION_JSON )
	public Collection<Acl> getAll() {
		return aclRepository.getAll();
	}

	@GET
	@Path( "{id:[0-9]*}" )
	@Produces( APPLICATION_JSON )
	public Acl get( @PathParam( "id" ) Long id ) {
		return aclRepository.findById( id );
	}

	@POST
	@Consumes( APPLICATION_JSON )
	@Produces( APPLICATION_JSON )
	public Acl matchAcl( Acl example ) {
		return aclRepository.findByExample( example );
	}
}
