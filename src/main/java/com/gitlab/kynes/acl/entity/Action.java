package com.gitlab.kynes.acl.entity;

public enum Action {
	ALLOW, DENY;
}
