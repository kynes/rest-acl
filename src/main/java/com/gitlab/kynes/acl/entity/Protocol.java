package com.gitlab.kynes.acl.entity;

public enum Protocol {
	TCP, UDP, ANY;
}
