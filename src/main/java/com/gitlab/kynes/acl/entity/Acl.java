package com.gitlab.kynes.acl.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@XmlRootElement
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType( XmlAccessType.FIELD )
public class Acl {
	private Long id;
	private String source;
	private String destination;
	private String protocol;
	@XmlTransient
	private Protocol protocolEnum;
	@XmlTransient
	private List<Long> ports;
	@XmlTransient
	private Action actionEnum;
	private String action;
}
