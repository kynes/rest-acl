package com.gitlab.kynes.acl.control;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.upperCase;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;

import org.apache.commons.lang3.tuple.Pair;

import com.gitlab.kynes.acl.entity.Acl;
import com.gitlab.kynes.acl.entity.Acls;
import com.gitlab.kynes.acl.entity.Action;
import com.gitlab.kynes.acl.entity.Protocol;
import com.gitlab.kynes.app.Config;

/**
 * Read ACL from file
 */
@Startup
@Singleton
public class AclReader {
	private static Pattern aclPatern = Pattern.compile( Config.ACL_REGEX );

	private Map<Long, Acl> acls;

	@Acls
	@Produces
	public Map<Long, Acl> getAcls() {
		if( acls == null ) {
			Stream<String> aclStream = new BufferedReader( new InputStreamReader( this.getClass()
				.getResourceAsStream( "/" + Config.ACL_FILE ) ) ).lines();
			acls = aclStream.map( this::parseAclLine )
				.filter( Objects::nonNull )
				.collect( toMap( Acl::getId, identity(), ( u, v ) -> u, LinkedHashMap::new ) );
		}

		return acls;
	}

	private Acl parseAclLine( String line ) {
		Matcher matcher = aclPatern.matcher( line );

		if( matcher.find() ) {
			try {
				Long id = Long.valueOf( matcher.group( "id" ) );
				Action action = Action.valueOf( upperCase( matcher.group( "action" ) ) );
				Pair<Protocol, List<Long>> protocol = parseProtocol( matcher.group( "protocol" ) );

				return new Acl( id, matcher.group( "source" ), matcher.group( "destination" ),
						matcher.group( "protocol" ), protocol.getLeft(), protocol.getRight(), action,
						matcher.group( "action" ) );
			} catch( IllegalArgumentException | NullPointerException e ) {
				System.out.println( "Error parsing line: " + line );
			}
		}

		return null;
	}

	public static Pair<Protocol, List<Long>> parseProtocol( String protocol ) throws IllegalArgumentException {
		if( isNotBlank( protocol ) ) {
			String[] parts = protocol.split( "/" );
			List<Long> ports = Collections.emptyList();

			if( parts.length > 1 && !equalsIgnoreCase( parts[1], Protocol.ANY.name() ) ) {
				ports = Arrays.stream( parts[1].split( "," ) )
					.map( Long::valueOf )
					.collect( toList() );
			}

			return Pair.of( Protocol.valueOf( upperCase( parts[0] ) ), ports );
		}
		return null;
	}
}
