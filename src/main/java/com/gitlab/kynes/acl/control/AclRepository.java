package com.gitlab.kynes.acl.control;

import static com.gitlab.kynes.acl.control.AclReader.parseProtocol;
import static com.gitlab.kynes.acl.entity.Protocol.ANY;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import javax.ejb.Singleton;
import javax.inject.Inject;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.net.util.SubnetUtils;

import com.gitlab.kynes.acl.entity.Acl;
import com.gitlab.kynes.acl.entity.Acls;
import com.gitlab.kynes.acl.entity.Protocol;

@Singleton
public class AclRepository {

	@Acls
	@Inject
	private Map<Long, Acl> acls;

	public Acl findById( Long id ) {
		return acls.get( id );
	}

	public Collection<Acl> getAll() {
		return acls.values();
	}

	public Acl findByExample( Acl example ) {
		return example == null ? null
				: acls.values()
					.stream()
					.filter( getConditions( example ) )
					.findFirst()
					.orElse( null );
	}

	private Predicate<Acl> getConditions( Acl example ) {
		ArrayList<Predicate<Acl>> conditions = new ArrayList<>();

		String source = example.getSource();
		if( isNotBlank( source ) && !isAny( source ) ) {
			conditions.add( acl -> isAny( acl.getSource() ) || inRange( acl.getSource(), source ) );
		}

		String destination = example.getDestination();
		if( isNotBlank( destination ) && !isAny( destination ) ) {
			conditions.add( acl -> isAny( acl.getDestination() ) || inRange( acl.getDestination(), destination ) );
		}

		try {
			Pair<Protocol, List<Long>> data = parseProtocol( example.getProtocol() );

			if( data != null ) {
				conditions.add( acl -> acl.getProtocolEnum() == ANY || acl.getProtocolEnum() == data.getLeft() );

				List<Long> ports = data.getRight();
				if( !ports.isEmpty() ) { // Unnecesary but a little bit faster
					conditions.add( acl -> acl.getProtocolEnum() == ANY || acl.getPorts()
						.containsAll( ports ) );
				}
			}
		} catch( IllegalArgumentException e ) {
			System.out.println( "Error parsing protocol: " + example.getProtocol() );
		}

		return conditions.stream()
			.reduce( Predicate::and )
			.orElse( t -> false );
	}

	private boolean inRange( String cidr, String address ) {
		SubnetUtils subnet = new SubnetUtils( fixCidr( cidr ) );
		subnet.setInclusiveHostCount( true );

		return subnet.getInfo()
			.isInRange( address );
	}

	private boolean isAny( String string ) {
		return isNotBlank( string ) && equalsIgnoreCase( string, ANY.name() );
	}

	private String fixCidr( String cidr ) {
		return isAny( cidr ) || cidr.contains( "/" ) ? cidr : cidr + "/32";
	}
}
