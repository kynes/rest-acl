package com.gitlab.kynes;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

import java.util.Collection;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gitlab.kynes.acl.control.AclRepository;
import com.gitlab.kynes.acl.entity.Acl;

@RunWith( Arquillian.class )
public class AclRepositoryTest extends SuiteDeployment {

	@Inject
	private AclRepository aclRepository;

	@Test
	@InSequence( 1 )
	public void getWrongArgmuent() {
		Acl acl = aclRepository.findById( null );

		assertThat( "Existance", acl, is( nullValue() ) );
	}

	@Test
	@InSequence( 2 )
	public void getNonExistant() {
		Acl acl = aclRepository.findById( 0L );

		assertThat( "Existance", acl, is( nullValue() ) );
	}

	@Test
	@InSequence( 3 )
	public void getFirstAcl() {
		Acl firstAcl = aclRepository.findById( 1L );

		assertThat( "First Acl not found", firstAcl, is( notNullValue() ) );
		assertThat( "Source cidr", firstAcl.getSource(), is( "43.0.0.0/8" ) );
		assertThat( "Destination cidr", firstAcl.getDestination(), is( "any" ) );
		assertThat( "Full Protocol", firstAcl.getProtocol(), is( "udp/53839,49944,58129,21778" ) );
		assertThat( "Action", firstAcl.getAction(), is( "deny" ) );
	}

	@Test
	@InSequence( 4 )
	public void allRetrieved() {
		Collection<Acl> acls = aclRepository.getAll();

		assertThat( "Acls exists", acls, is( notNullValue() ) );
		assertThat( "Acls size", acls, hasSize( 1000 ) );
	}

	@Test
	@InSequence( 5 )
	public void checkFirstRetrieved() {
		Collection<Acl> acls = aclRepository.getAll();

		Acl firstAcl = acls.iterator()
			.next();
		assertThat( "First Acl not found", firstAcl, is( notNullValue() ) );
		assertThat( "Order", firstAcl.getId(), is( 1L ) );
		assertThat( "Source cidr", firstAcl.getSource(), is( "43.0.0.0/8" ) );
		assertThat( "Destination cidr", firstAcl.getDestination(), is( "any" ) );
		assertThat( "Full Protocol", firstAcl.getProtocol(), is( "udp/53839,49944,58129,21778" ) );
		assertThat( "Action", firstAcl.getAction(), is( "deny" ) );
	}

	@Test
	@InSequence( 6 )
	public void exampleNull() {
		Acl acl = aclRepository.findByExample( null );

		assertThat( "Existance", acl, is( nullValue() ) );
	}

	@Test
	@InSequence( 7 )
	public void exampleNullValues() {
		Acl example = new Acl( null, null, null, null, null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Existance", acl, is( nullValue() ) );
	}

	@Test
	@InSequence( 8 )
	public void exampleNullSource() {
		Acl example = new Acl( null, null, "127.0.0.1", "udp/49944", null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 1L ) );
	}

	@Test
	@InSequence( 9 )
	public void exampleNullDestination() {
		Acl example = new Acl( null, "43.0.0.2", null, null, null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 1L ) );
	}

	@Test
	@InSequence( 10 )
	public void exampleFilterSource() {
		Acl example = new Acl( null, "43.0.0.2", "127.0.0.1", "udp/49944", null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 1L ) );
	}

	@Test
	@InSequence( 11 )
	public void exampleFilterSourceWrongProtocol() {
		Acl example = new Acl( null, "43.0.0.2", "127.0.0.1", "test/0000", null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 1L ) );
	}

	@Test
	@InSequence( 12 )
	public void exampleFilterSourceWrongPort() {
		Acl example = new Acl( null, "43.0.0.2", "127.0.0.1", "udp/0000", null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 658L ) );
	}

	@Test
	@InSequence( 13 )
	public void exampleFilterDestination() {
		Acl example = new Acl( null, "127.0.0.1", "210.28.68.44", "udp/1990", null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 3L ) );
	}

	@Test
	@InSequence( 14 )
	public void exampleFilterDestinationWrongPort() {
		Acl example = new Acl( null, "127.0.0.1", "210.28.68.44", "udp/0000", null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 658L ) );
	}

	@Test
	@InSequence( 15 )
	public void exampleFilterAll() {
		Acl example = new Acl( null, "208.110.31.137", "204.136.246.38", "tcp/17980", null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 13L ) );
	}

	@Test
	@InSequence( 16 )
	public void exampleFilterBothAnyPort() {
		Acl example = new Acl( null, "245.200.223.5", "232.0.0.7", "udp/0000", null, null, null, null );
		Acl acl = aclRepository.findByExample( example );

		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 16L ) );
	}
}
