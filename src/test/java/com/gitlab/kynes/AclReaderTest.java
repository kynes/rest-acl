package com.gitlab.kynes;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gitlab.kynes.acl.entity.Acl;
import com.gitlab.kynes.acl.entity.Acls;
import com.gitlab.kynes.acl.entity.Action;
import com.gitlab.kynes.acl.entity.Protocol;

@RunWith( Arquillian.class )
public class AclReaderTest extends SuiteDeployment {

	@Acls
	@Inject
	private Map<Long, Acl> acls;

	@Test
	@InSequence( 1 )
	public void loadCorrectNumberOfAcls() {
		assertThat( "Acls loaded", acls, is( notNullValue() ) );
		assertThat( "Acls size", acls.values(), hasSize( 1000 ) );
	}

	@Test
	@InSequence( 2 )
	public void firstAcl() {
		Optional<Acl> firstAcl = acls.values()
			.stream()
			.filter( a -> a.getId()
				.equals( 1L ) )
			.findFirst();

		assertThat( "Order with Id 1 present", firstAcl.isPresent(), is( true ) );

		Acl acl = firstAcl.get();
		assertThat( "Source cidr", acl.getSource(), is( "43.0.0.0/8" ) );
		assertThat( "Destination cidr", acl.getDestination(), is( "any" ) );
		assertThat( "Full Protocol", acl.getProtocol(), is( "udp/53839,49944,58129,21778" ) );
		assertThat( "Protocol", acl.getProtocolEnum(), is( Protocol.UDP ) );
		assertThat( "Ports", acl.getPorts(), is( Arrays.asList( 53839L, 49944L, 58129L, 21778L ) ) );
		assertThat( "Action", acl.getAction(), is( "deny" ) );
		assertThat( "Action enum", acl.getActionEnum(), is( Action.DENY ) );
	}

}
