package com.gitlab.kynes;

import java.io.File;

import org.eu.ingwar.tools.arquillian.extension.suite.annotations.ArquillianSuiteDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

import com.gitlab.kynes.app.Config;

@ArquillianSuiteDeployment
public class SuiteDeployment {

	@Deployment
	public static WebArchive createDeployment() {
		File[] pomLibraries = Maven.resolver()
			.loadPomFromFile( "pom.xml" )
			.importRuntimeAndTestDependencies()
			.resolve()
			.withTransitivity()
			.asFile();

		WebArchive archive = ShrinkWrap.create( WebArchive.class, Config.APP_NAME + ".war" )
			.addAsWebInfResource( EmptyAsset.INSTANCE, "beans.xml" )
			.addPackages( true, "com.gitlab.kynes" )
			.addAsResource( Config.ACL_FILE )
			.addAsLibraries( pomLibraries );
		return archive;
	}
}
