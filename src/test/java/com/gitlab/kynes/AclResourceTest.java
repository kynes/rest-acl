package com.gitlab.kynes;

import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Collection;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gitlab.kynes.acl.entity.Acl;
import com.gitlab.kynes.app.Config;

@RunWith( Arquillian.class )
public class AclResourceTest extends SuiteDeployment {
	private WebTarget target;

	@ArquillianResource
	private URL base;

	@Before
	public void setUp() throws MalformedURLException {
		Client client = ClientBuilder.newClient();
		target = client.target( URI.create( new URL( base, Config.ACL_EP ).toExternalForm() ) );
		target.register( Acl.class );
	}

	@Test
	@InSequence( 1 )
	public void getWrongArgmuent() {
		Response response = target.path( "alpha" )
			.request( APPLICATION_JSON )
			.get();
		assertThat( "Status", response.getStatusInfo(), is( NOT_FOUND ) );
	}

	@Test
	@InSequence( 2 )
	public void getNonExistant() {
		Response response = target.path( "0" )
			.request( APPLICATION_JSON )
			.get();
		assertThat( "Status", response.getStatusInfo(), is( NO_CONTENT ) );
	}

	@Test
	@InSequence( 3 )
	public void getFirstAcl() {
		Acl firstAcl = target.path( "1" )
			.request( APPLICATION_JSON )
			.get( Acl.class );

		assertThat( "First Acl not found", firstAcl, is( notNullValue() ) );
		assertThat( "Source cidr", firstAcl.getSource(), is( "43.0.0.0/8" ) );
		assertThat( "Destination cidr", firstAcl.getDestination(), is( "any" ) );
		assertThat( "Full Protocol", firstAcl.getProtocol(), is( "udp/53839,49944,58129,21778" ) );
		assertThat( "Action", firstAcl.getAction(), is( "deny" ) );
	}

	@Test
	@InSequence( 4 )
	public void allRetrieved() {
		Response response = target.request( APPLICATION_JSON )
			.get();
		assertThat( "Status", response.getStatusInfo(), is( OK ) );

		Collection<Acl> acls = response.readEntity( new GenericType<Collection<Acl>>() {} );
		assertThat( "Acls size", acls, hasSize( 1000 ) );
	}

	@Test
	@InSequence( 5 )
	public void checkFirstRetrieved() {
		Response response = target.request( APPLICATION_JSON )
			.get();
		assertThat( "Status", response.getStatusInfo(), is( OK ) );

		Collection<Acl> acls = response.readEntity( new GenericType<Collection<Acl>>() {} );

		Acl firstAcl = acls.iterator()
			.next();
		assertThat( "First Acl not found", firstAcl, is( notNullValue() ) );
		assertThat( "Order", firstAcl.getId(), is( 1L ) );
		assertThat( "Source cidr", firstAcl.getSource(), is( "43.0.0.0/8" ) );
		assertThat( "Destination cidr", firstAcl.getDestination(), is( "any" ) );
		assertThat( "Full Protocol", firstAcl.getProtocol(), is( "udp/53839,49944,58129,21778" ) );
		assertThat( "Action", firstAcl.getAction(), is( "deny" ) );
	}

	@Test
	@InSequence( 6 )
	public void exampleNull() {
		Response response = target.request( APPLICATION_JSON )
			.accept( APPLICATION_JSON )
			.post( entity( null, APPLICATION_JSON ) );

		assertThat( "Status", response.getStatusInfo(), is( NO_CONTENT ) );
	}

	@Test
	@InSequence( 7 )
	public void exampleNullValues() {
		Acl example = new Acl( null, null, null, null, null, null, null, null );
		Response response = target.request( APPLICATION_JSON )
			.accept( APPLICATION_JSON )
			.post( entity( example, APPLICATION_JSON ) );

		assertThat( "Status", response.getStatusInfo(), is( NO_CONTENT ) );
	}

	@Test
	@InSequence( 8 )
	public void exampleFilterSource() {
		Acl example = new Acl( null, "43.0.0.2", "127.0.0.1", "udp/49944", null, null, null, null );
		Response response = target.request( APPLICATION_JSON )
			.accept( APPLICATION_JSON )
			.post( entity( example, APPLICATION_JSON ) );
		assertThat( "Status", response.getStatusInfo(), is( OK ) );

		Acl acl = response.readEntity( Acl.class );
		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 1L ) );
	}

	@Test
	@InSequence( 9 )
	public void exampleFilterSourceWrongPort() {
		Acl example = new Acl( null, "43.0.0.2", "127.0.0.1", "udp/0000", null, null, null, null );
		Response response = target.request( APPLICATION_JSON )
			.accept( APPLICATION_JSON )
			.post( entity( example, APPLICATION_JSON ) );

		assertThat( "Status", response.getStatusInfo(), is( OK ) );

		Acl acl = response.readEntity( Acl.class );
		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 658L ) );
	}

	@Test
	@InSequence( 10 )
	public void exampleFilterDestination() {
		Acl example = new Acl( null, "127.0.0.1", "210.28.68.44", "udp/1990", null, null, null, null );
		Response response = target.request( APPLICATION_JSON )
			.accept( APPLICATION_JSON )
			.post( entity( example, APPLICATION_JSON ) );
		assertThat( "Status", response.getStatusInfo(), is( OK ) );

		Acl acl = response.readEntity( Acl.class );
		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 3L ) );
	}

	@Test
	@InSequence( 11 )
	public void exampleFilterDestinationWrongPort() {
		Acl example = new Acl( null, "127.0.0.1", "210.28.68.44", "udp/0000", null, null, null, null );
		Response response = target.request( APPLICATION_JSON )
			.accept( APPLICATION_JSON )
			.post( entity( example, APPLICATION_JSON ) );

		assertThat( "Status", response.getStatusInfo(), is( OK ) );

		Acl acl = response.readEntity( Acl.class );
		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 658L ) );
	}

	@Test
	@InSequence( 12 )
	public void exampleFilterAll() {
		Acl example = new Acl( null, "208.110.31.137", "204.136.246.38", "tcp/17980", null, null, null, null );
		Response response = target.request( APPLICATION_JSON )
			.accept( APPLICATION_JSON )
			.post( entity( example, APPLICATION_JSON ) );
		assertThat( "Status", response.getStatusInfo(), is( OK ) );

		Acl acl = response.readEntity( Acl.class );
		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 13L ) );
	}

	@Test
	@InSequence( 13 )
	public void exampleFilterBothAnyPort() {
		Acl example = new Acl( null, "245.200.223.5", "232.0.0.7", "udp/0000", null, null, null, null );
		Response response = target.request( APPLICATION_JSON )
			.accept( APPLICATION_JSON )
			.post( entity( example, APPLICATION_JSON ) );
		assertThat( "Status", response.getStatusInfo(), is( OK ) );

		Acl acl = response.readEntity( Acl.class );
		assertThat( "Acl exists", acl, is( notNullValue() ) );
		assertThat( "Order", acl.getId(), is( 16L ) );
	}
}
