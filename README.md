# Prueba API REST 2017

## Descripción
Este proyecto es la prueba técnica que comprueba la forma de solucionar el problema presentado.

## Cómo ejecutarlo
Para ejecutar el servicio:
```shell
mvn wildfly:run -Dmaven.test.skip=true
```

Para ejecutar las pruebas:
```shell
mvn test
```

En ambos casos deben estar libres los puertos 8080, 8443 y 9990.

## Arquitectura
La solución técnica elegida consiste en la siguiente:
1. [Java EE](https://github.com/javaee): Estandar para el desarrollo de aplicaciones web que facilita el trabajo. De todos los estándares que forman parte de éste, sólo usamos los siguientes:
  * [Inyección de dependencias](http://javax-inject.github.io/javax-inject/): Nos permite invertir el control de la gestión de dependencias.
  * [EJB3](https://github.com/javaee/javax.ejb): Usado para gestionar beans durante el arranque del servidor.
  * [JAX-RS](https://github.com/jax-rs): Api que permite la simplificación de la definición de servicios web.
1. Pruebas: Las herramientas elegidas para realizar las pruebas son:
  * [JUnit](http://junit.org/junit4/): Nos permite integrar las pruebas fácilmente en Eclipse.
  * [Arquillian] (http://arquillian.org/): Usado para lanzar las pruebas en un contenedor Java EE, y así replicar las condiciones reales de trabajo.
  * [Hamcrest](http://hamcrest.org/JavaHamcrest/): Framework que permite que las pruebas reflejen su intención.
1. Otros
  * [Lombok](https://projectlombok.org/): Permite eliminar código redundante.
  
## Solución propuesta
Para resolver el problema lo hemos dividido en tres partes: 
1. Un proceso que parsea el fichero y lo guarda en memoria
1. Un servicio que filtra los datos en memoria
1. Un servicio web modelado como un API REST que accede al servicio de los datos

Procesamos el fichero de forma secuencial y lo guardamos en un LinkedHashMap, el cual mantiene el orden de inserción, y cuya clave es el orden de la regla indicado en el fichero; de esta forma podemos acceder directamente a una regla concreta y además al filtrar mediante el api Stream de Java 8 nos aseguramos que se recorren los valores en el orden apropiado a la hora de devolver la primera regla a buscar.

Para filtrar los datos en memoria optamos por generar dinámicamente una serie de Predicados en función de los datos de entrada del servicio web y aplicarlos al stream de las reglas.

El servicio web se limita a hacer de intermediario entre el usuario y el servicio de datos, modelado como un api REST indicado.

## Detalles de implementación:

#### Get the ACL
 * Description: Returns the whole ACL
 * Endpoint: **http://localhost:8080/intelliment/acl**
 * Operation: GET
 * Example response:
 
```javascript
      [{
        "id": 1,
        "source": "192.168.0.10",
        "destination": "192.168.0.2",
        "protocol": "tcp/80",
        "action": "allow"
      },{
        "id": 2,
        "source": "88.1.12.225",
        "destination": "99.235.1.15",
        "protocol": "tcp/80,8080",
        "action": "deny"
      }]
```

#### Get a single rule
 * Description: Returns the single rule given the id
 * Endpoint: **http://localhost:8080/intelliment/acl/:id**
 * Operation: GET
 * Example response:
 
```javascript
    {
    "id": 1,
    "source": "192.168.0.10",
    "destination": "192.168.0.2",
    "protocol": "tcp/80",
    "action": "allow"
    }
```

#### Get the matched rule for an specific packet
 * Description: Return the first rule that their fields match with the packet
fields in order to know what action to apply. For example, for a packet
with fields: [source=”192.168.0.5”, destination=”192.168.0.1” and
protocol=”UDP/80”] the first rule that match it will be the number 3.
 * Endpoint: **http://localhost:8080/intelliment/acl**
 * Operation: GET
 * Example request body:
 
```javascript
    {
      "source": "192.168.0.5",
      "destination": "192.168.0.1",
      "protocol": "udp/80"
    }
```

 * Example response:

```javascript
    {
      "id": 3,
      "source": "192.168.0.0/24",
      "destination": "192.168.0.0/28",
      "protocol": "udp/any",
      "action": "allow"
    }
```


## Consideraciones descartadas:
* Descartamos el uso de la base de datos integrada en el servidor ya que no aporta al proceso y aumentaba la complejidad.
* Descartamos el procesamiento en paralelo del stream ya que en las pruebas de rendimiento realizadas la diferencia de tiempo era mínima y se perdía al reordenar el stream.